import Koa from 'koa'
import Router from '@koa/router'
import {Sequelize, DataTypes} from 'sequelize'
import bodyParser from 'koa-bodyparser';

const app = new Koa();
const router = new Router();
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './account.db'
})

app.use(bodyParser())

const Account = sequelize.define("Account", {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    username: {
        type: DataTypes.TEXT,
        unique: true,
        allowNull: false
    },
    password: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    point: {
        type: DataTypes.INTEGER,
        defaultValue: 0

    },
    group: {
        type: DataTypes.TEXT,
        defaultValue: null
    }
}, {
    tableName: 'accounts'
})

const Error = sequelize.define("Error", {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    username: {
        type: DataTypes.TEXT
    },
    password: {
        type: DataTypes.TEXT
    }
}, {
    tableName: "errors"
})
await Account.sync({force: true})
await Error.sync({force: true})
// const test = await Account.create({
//     username: 'c',
//     password: '123'

// })
// await test.save()

router.post("/create", async (ctx, next) => {
    const user = ctx.request.body.username
    const password = ctx.request.body.password
    const group = ctx.request.body.group
    // if (Account.findAll({
    //     where: {
    //         username: user
    //     }
    // }))
    var a = await Account.findAll({
        where: {
            username: user
        }
    })
    if (a.length == 0) {
        const account = Account.create({
            username: user,
            password: password,
            group: group
        })
        // ;(await account).save()
        // await account.save()
        ctx.body = "Successful"
        ctx.status = 200
        // await next()
    }
    else {
        ctx.status = 400
        ctx.body = "User exist"
    }
    await next()
    // console.log(Account.findAll())
})

router.post("/update", async (ctx, next) => {
    // ctx.body = ctx.request.body
    const user = ctx.request.body.username
    const point = ctx.request.body.point
    var a = await Account.findAll({
        where: {
            username: user
        }
    })
    if (a.length != 0) {
        await Account.update({point: point}, {
            where: {
                username: user
            }
        })
        ctx.body = "Successful"
        ctx.status = 200
    }
    else {
        ctx.body = "Not found"
        ctx.status = 400
    }
    await next()
})


router.get("/getUser/:user", async (ctx,next) => {
    const user = ctx.params.user
    var a = await Account.findAll({
        where: {
            username: user
        }
    })
    if (a.length != 0) {
        ctx.body = a[0].username + "|" + a[0].password + "|" + a[0].point
        ctx.status = 200
    }
    else {
        ctx.body = "Not found"
        ctx.status = 400
    }
    await next()
})


router.get("/getGroup/:group", async (ctx, next) => {
    const group = ctx.params.group
    const a = await Account.findAll({
        where: {
            group: group
        }
    })
    if (a.length != 0) {
        ctx.body = ''
        for (const account of a) {
            ctx.body += account.username + "|" + account.password + "|" + account.point + "\n"
        }
        ctx.status = 200
    }
    else {
        ctx.body = "Not found"
        ctx.status = 400
    }
    await next()
})

router.get("/listGroup", async (ctx, next) => {
    const a = await Account.findAll({
        attributes: ['group']
    })
    // console.log(JSON.stringify(a))
    // ctx.body = ''
    // for (const group of a) {
    //     ctx.body += group.group + "|"
    // }
    ctx.body = JSON.stringify(a)
    await next()
})

router.get("/clearGroup/:group", async (ctx, next) => {
    const group = ctx.params.group
    await Account.destroy({
        where: {
            group: group
        }
    })
     
    await next()
})

router.get("/error", async (ctx, next) => {
    var a = await Error.findAll()
    ctx.body = JSON.stringify(a)
    await next()
})

router.post("/error", async (ctx, next) => {
    const username = ctx.body.username
    const error = ctx.body.error
    Error.create({
        username: username,
        error: error
    })
    ctx.body = ''
    await next()

})

app.use(router.routes()).use(router.allowedMethods())

app.listen(3000)


// router.get("/hello", async (ctx, next) => {
//     ctx.body = "Hello world!"
//     await next()
// })

// // app.use(async ctx => {
// //     ctx.body = "Hello world!"
// // })

// app.use(router.routes()).use(router.allowedMethods())

// app.listen(3000)